"use strict"

console.log("Hello from Cloud Run");

var exports = module.exports = function(name) {

    if (typeof name == 'object') {
        if (Object.keys(name).length === 1 && name[Object.keys(name)[0]] === "") {
            name = name[Object.keys(name)[0]];
        } else {
            name = JSON.stringify(name);
        }
    } else if (typeof name !== 'string') {
        console.log('Got argument type', typeof name, ':', name);
        name = 'error: Unexpected argument type ' + (typeof name);
    }

    return name;

};
